/*This file is part of tilefish

    Copyright (C) 2019 Asko Ropponen

    tilefish is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    tilefish is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with tilefish.  If not, see <https://www.gnu.org/licenses/>.*/
import QtQuick 2.0
import Sailfish.Silica 1.0
import harbour.okxa 1.0

import "../../js/AboutFetch.js" as AboutFetch

Page {
    id: pageAbout
    SilicaFlickable {
        id: flickable1
        anchors.fill: parent
        contentWidth: parent.width
        property real innerheight: topCol.height + bottomCol.height
        contentHeight: innerheight + innerheight / 16
        ScrollDecorator {  }
        Column {
            id: topCol
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.leftMargin: Theme.horizontalPageMargin
            anchors.rightMargin: Theme.horizontalPageMargin
            spacing: Theme.iconSizeSmall
            PageHeader {
                id: header
                //: About page name
                //% "About "
                title: qsTrId("tilefish-about") + app.appName
            }
            Row {
                anchors.horizontalCenter: parent.horizontalCenter
                height: logo.height
                anchors.left: parent.left
                spacing: Theme.iconSizeMedium
                Image {
                    id: logo
                    anchors.verticalCenter: parent.verticalCenter
                    width: Theme.itemSizeExtraLarge
                    height: Theme.itemSizeExtraLarge
                    source: "/usr/share/icons/hicolor/172x172/apps/" + app.execName + ".png"
                }
                Column {
                    anchors.verticalCenter: parent.verticalCenter
                    Label {
                        text: app.appName
                        font.pixelSize: Theme.fontSizeExtraLarge
                        color: Theme.highlightColor
                    }
                    Label {
                        text: app.execName
                    }
                }
            }
            Label {
                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                //: Application Description on About page
                //% "Image viewer with multiple images on multiple pages for Sailfish OS."
                text: qsTrId("tilefish-about-description")
                font.pixelSize: Theme.fontSizeMedium
                color: Theme.highlightColor
            }
            Label {
                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: Theme.fontSizeMedium
                color: Theme.highlightColor
                // call getCopyrightAuthors, 1st argument is this label, second is the years shown
                Component.onCompleted: AboutFetch.getCopyrightAuthors(this, "2019")
            }
            ComboBox {
                id: licencebox
                width: parent.width
                //: Show Licence button text
                //% "Show Licence"
                label: qsTrId("tilefish-licence") + ":"
                value: "GPLv3"
                menu: ContextMenu {
                    MenuItem {
                        elide: Text.ElideNone
                        Component.onCompleted: licencebox.currentIndex = -1
                        onClicked: {
                            licencebox.currentIndex = -1
                            licencebox.value = "GPLv3"
                        }
                        wrapMode: Text.WordWrap
                        height: 1000
                        text: "tilefish is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. \n\ntilefish is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details. \n\nYou should have received a copy of the GNU General Public License along with tilefish.  If not, see <https://www.gnu.org/licenses/>"
                    }
                }
            }
        }
        Column {
            id: bottomCol
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: topCol.bottom
            anchors.topMargin: Theme.horizontalPageMargin
            anchors.leftMargin: Theme.horizontalPageMargin
            anchors.rightMargin: Theme.horizontalPageMargin
            Grid {
                anchors.left: parent.left
                width: parent.width
                columns: 2
                Label {
                    //: Source (code) text on about page
                    //% "Source"
                    text: qsTrId("tilefish-about-source") + ": "
                    width: parent.width / 4
                    horizontalAlignment: Text.AlignRight
                    font.pixelSize: Theme.fontSizeMedium
                    color: Theme.highlightColor
                }
                Label {
                    id: column1
                    width: parent.width - x
                    font.pixelSize: Theme.fontSizeMedium
                    wrapMode: Text.WordWrap
                    text: "https://gitlab.com/Okxa/tilefish-sfos"
                }
                Label {
                    //: Authors text on about page
                    //% "Authors"
                    text: qsTrId("tilefish-about-authors") + ": "
                    width: parent.width / 4
                    horizontalAlignment: Text.AlignRight
                    font.pixelSize: Theme.fontSizeMedium
                    color: Theme.highlightColor
                }
                Label {
                    width: parent.width - x
                    textFormat: Text.RichText
                    font.pixelSize: Theme.fontSizeMedium
                    Component.onCompleted: AboutFetch.getAuthors(this)
                }
                Label {
                    //: Translators text on about page
                    //% "Translators"
                    text: qsTrId("tilefish-about-translator") + ": "
                    width: parent.width / 4
                    horizontalAlignment: Text.AlignRight
                    font.pixelSize: Theme.fontSizeMedium
                    color: Theme.highlightColor
                }
                Label {
                    width: parent.width - x
                    textFormat: Text.RichText
                    font.pixelSize: Theme.fontSizeMedium
                    Component.onCompleted: AboutFetch.getTranslators(this, localeHelper)
                }
            }
            LocaleHelper {
                id: localeHelper
            }
        }
    }
}
