# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TARGET = tilefish

CONFIG += sailfishapp

SOURCES += src/tilefish.cpp \
    src/localehelper.cpp

DISTFILES += qml/tilefish.qml \
    qml/cover/CoverPage.qml \
    translations/*.ts \
    qml/model/ViewPageListModel.qml \
    qml/pages/MainPage.qml \
    qml/components/ViewPageComponent.qml \
    js/pageManager.js \
    js/imageViewManager.js \
    qml/pages/AddPage.qml \
    qml/pages/SelectFilePage.qml \
    js/addPageSliderEqualizer.js \
    rpm/tilefish.changes.in \
    rpm/tilefish.changes.run.in \
    rpm/tilefish.spec \
    rpm/tilefish.yaml \
    tilefish.desktop \
    qml/pages/AboutPage.qml \
    js/AboutFetch.js

SAILFISHAPP_ICONS = 86x86 108x108 128x128 172x172

# to disable building translations every time, comment out the
# following CONFIG line
CONFIG += sailfishapp_i18n \
          sailfishapp_i18n_idbased

#translations
# to add new translation, add it here
# and use shellscript included to update files
# then just use qlinquist or edit manually.
TRANSLATIONS += translations/tilefish.ts \
                translations/tilefish-fi.ts \
                translations/tilefish-zh_CN.ts \
                translations/tilefish-fr.ts \
                translations/tilefish-sv.ts

#hack to allow lupdate see the .ts files
lupdate_only{
SOURCES = *.qml \
          *.js \
          qml/*.qml \
          qml/components/*.qml \
          qml/cover/*.qml \
          qml/model/*.qml \
          qml/pages/*.qml \
          js/*.js
}

HEADERS += \
    src/localehelper.h

js.path = /usr/share/tilefish/js
js.files = js/*

INSTALLS += js

RESOURCES += \
    ui.qrc
