/*This file is part of tilefish

    Copyright (C) 2019 Asko Ropponen

    tilefish is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    tilefish is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with tilefish.  If not, see <https://www.gnu.org/licenses/>.*/
function addPage(title, pageItems, columns, rows) {
    if (pageItems) {
        var nextPage = viewPageContainer.model.count + 1
        viewPageContainer.model.append({
                                        pageName: title + " " + nextPage,
                                        pageId: "page" + nextPage,
                                        pageItems: pageItems,
                                        pageImages: 0,
                                        pageColumns: columns,
                                        pageRows: rows
                                       })
        changePage(viewPageContainer.model.count - 1)
    }
}
function addImageTile(grid, pageItems, columns, rows) {
    for (var i = 0; i < pageItems; i++) {
        var comp = Qt.createComponent("../qml/components/ViewPageComponent.qml")
        var obj = comp.createObject(grid, {width: page.width / columns, height: realheight / rows })
    }
}
// removal
function removeCurrentPage() {
    removePage(viewPageContainer.model.currentIndex)
}
function removePage(index) {
    if (viewPageContainer.model.count > 0) {
        viewPageContainer.model.remove(index)
    }
    if (index === 0) {
        changePage(0)
    }
    else {
        prevPage()
    }
}
//misc
function updateTitle(index) {
    var title
    var coverCurrentPage
    var coverCurrentPanes
    var coverCurrentImages
    // when no pages
    if (viewPageContainer.model.count === 0) {
        title = ""
        coverCurrentPage = app.noPagesCoverText
        coverCurrentPanes = ""
        coverCurrentImages = ""
    }
    else {
        title = (index + 1) + " / " + viewPageContainer.model.count
        coverCurrentPage = app.pageNameText + ": " + title
        coverCurrentPanes = app.panesText + ": " + viewPageContainer.model.get(index).pageItems
        coverCurrentImages = app.imagesText + ": " + viewPageContainer.model.get(index).pageImages
    }
    header.title = title
    // set applicationWindows' currentpage, used in coverpage
    app.currentPage = coverCurrentPage
    app.currentPagePanes = coverCurrentPanes
    app.currentPageImages = coverCurrentImages
}

// nav
function nextPage() {
    if (viewPageContainer.model.currentIndex === viewPageContainer.model.count - 1) {
        changePage(0)
    }
    else {
        changePage(viewPageContainer.model.currentIndex + 1)
    }
}
function prevPage() {
    if (viewPageContainer.model.currentIndex <= 0) {
        changePage(viewPageContainer.model.count - 1)
    }
    else {
        changePage(viewPageContainer.model.currentIndex - 1)
    }
}
function changePage(index) {
    updateTitle(index)
    viewPageContainer.model.currentIndex = index
}
