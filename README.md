# Tilefish - A Sailfish OS application

Image viewer with multiple images on multiple pages for Sailfish OS.

You can get it from Jolla's Harbour Store.

Or from [OpenRepos](https://openrepos.net/content/okxa/tilefish)

Or if you want to build it yourself, use the Sailfish SDK and QtCreator.

## Features

- Open up to 16 images simultaneously on one page.
- Zoom, rotate and flip the images invidually.

16 images on one page might not be usefull on phones, maybe on larger screen devices.

## Translations?

If you want to translate this app:

It is easier with lupdate and QtLinquist.

To create a translation file

1. Fork this repo ([master] branch) (and clone the fork to your local machine)
2. Add your translation file in `tilefish.pro`

        TRANSLATIONS += translations/tilefish.ts \
                        translations/tilefish-fi.ts

        TRANSLATIONS += translations/tilefish.ts \
                        translations/tilefish-fi.ts \
                        translations/tilefish-de.ts

3. Run translationfile.sh OR copy and rename the file manually.
4. Open the file in QtLinquist or edit the file manually. QtLinquist is easier.
5. Add your credits to `res/translators.json` if you want to show them in the About -page (A pseudonym or whatever is fine)

```json
{
	"translations": [
		{"language": "fi", "translators": ["Okxa"]}, <--- HOX! JSON objects/arrays are separated by dots!
		{"language": "de", "translators": ["A new contributor!"]} <--- But not the last one!
	]
}
```
If Multiple translators have contributed to single language:

```json
{
	"translations": [
		{"language": "fi", "translators": ["Okxa"]},
		{"language": "de", "translators": ["A new contributor!", "Another Translator"]}
	]
}
```
After all that is done and you are sure that everything is correct, push changes to your fork and submit a pull request.

I will then review & test that it works (You can also test if you have the SDK.) and then build & release new versions.

## Licence

License: GNU General Public License (GNU GPLv3)

Copyright 2019 Asko Ropponen


