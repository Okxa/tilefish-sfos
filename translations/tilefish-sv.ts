<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv">
<context>
    <name></name>
    <message id="tilefish-no-image-loaded">
        <location filename="../qml/components/ViewPageComponent.qml" line="+65"/>
        <source>No Image Loaded. Tap to Load Image.</source>
        <extracomment>Shown in image pane when no image loaded.</extracomment>
        <translation>Ingen bild inläst. Tryck för att läsa in en bild.</translation>
    </message>
    <message id="tilefish-remorse-clear">
        <location line="+92"/>
        <source>Clearing view</source>
        <oldsource>Clearing view...</oldsource>
        <extracomment>Remorse popup text when removing image from view</extracomment>
        <translation>Rensar vyn</translation>
    </message>
    <message id="tilefish-coverlabel">
        <location filename="../qml/tilefish.qml" line="+29"/>
        <source>Tilefish</source>
        <oldsource>TileFish</oldsource>
        <extracomment>App name</extracomment>
        <translation>Tilefish</translation>
    </message>
    <message id="tilefish-add-page">
        <location filename="../qml/pages/AddPage.qml" line="+36"/>
        <source>Add Page...</source>
        <extracomment>Add Page -text.</extracomment>
        <translation>Lägg till sida</translation>
    </message>
    <message id="tilefish-columns">
        <location line="+5"/>
        <source>Columns</source>
        <extracomment>Columns -slider label</extracomment>
        <translation>Kolumner</translation>
    </message>
    <message id="tilefish-rows">
        <location line="+13"/>
        <source>Rows</source>
        <extracomment>Rows -slider label</extracomment>
        <translation>Rader</translation>
    </message>
    <message id="tilefish-panes">
        <location filename="../qml/tilefish.qml" line="+13"/>
        <source>Panes</source>
        <extracomment>Panes as in &quot;Panes&quot;: 4</extracomment>
        <translation>Paneler</translation>
    </message>
    <message id="tilefish-page-title-text">
        <location line="-4"/>
        <source>Page</source>
        <extracomment>prefix for Pages. eq. &quot;Page&quot; 1 / *</extracomment>
        <translation>Sida</translation>
    </message>
    <message id="tilefish-images">
        <location line="+8"/>
        <source>Images</source>
        <extracomment>prefix for images. eq. &quot;Images&quot; : 4</extracomment>
        <translation>Bilder</translation>
    </message>
    <message id="tilefish-page-remove">
        <location filename="../qml/pages/MainPage.qml" line="+101"/>
        <source>Removing page</source>
        <oldsource>Removing Page...</oldsource>
        <extracomment>Remorse popup text when removing a page</extracomment>
        <translation>Tar bort sida</translation>
    </message>
    <message id="tilefish-no-pages">
        <location line="+105"/>
        <source>No image pages, try adding one by pressing the + button above.</source>
        <extracomment>When no pages are added</extracomment>
        <translation>Inga bildsidor. Försök lägga till en, genom att trycka på plusknappen ovan.</translation>
    </message>
    <message id="tilefish-goto-about">
        <location line="+6"/>
        <source>About this software...</source>
        <extracomment>Go to About page -button</extracomment>
        <translation>Om denna mjukvata...</translation>
    </message>
    <message id="tilefish-cover-no-pages">
        <location filename="../qml/tilefish.qml" line="-11"/>
        <source>No Pages.</source>
        <extracomment>Shown on Cover when no pages exists</extracomment>
        <translation>Inga sidor.</translation>
    </message>
    <message id="tilefish-about">
        <location filename="../qml/pages/AboutPage.qml" line="+44"/>
        <source>About </source>
        <oldsource>About Tilefish</oldsource>
        <extracomment>About page name</extracomment>
        <translation>Om </translation>
    </message>
    <message id="tilefish-about-description">
        <location line="+34"/>
        <source>Image viewer with multiple images on multiple pages for Sailfish OS.</source>
        <extracomment>Application Description on About page</extracomment>
        <translation>Bildvisare med flera bilder på flera sidor, för Sailfish OS.</translation>
    </message>
    <message id="tilefish-licence">
        <location line="+20"/>
        <source>Show Licence</source>
        <oldsource>Licence</oldsource>
        <extracomment>Show Licence button text</extracomment>
        <translation>Visa licens</translation>
    </message>
    <message id="tilefish-about-source">
        <location line="+32"/>
        <source>Source</source>
        <extracomment>Source (code) text on about page</extracomment>
        <translation>Källkod</translation>
    </message>
    <message id="tilefish-about-authors">
        <location line="+16"/>
        <source>Authors</source>
        <oldsource>Source</oldsource>
        <extracomment>Authors text on about page</extracomment>
        <translation>Utvecklare</translation>
    </message>
    <message id="tilefish-about-translator">
        <location line="+15"/>
        <source>Translators</source>
        <extracomment>Translators text on about page</extracomment>
        <translation>Översättare</translation>
    </message>
</context>
</TS>
